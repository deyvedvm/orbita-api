import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UsersModule } from './users/users.module';
import { ConfigModule } from './config/config.module';

import { User } from './users/user.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'username',
      password: 'password',
      database: 'orbita-api-dev',
      entities: [User],
      synchronize: true,
    }),
    UsersModule,
    ConfigModule],
  controllers: [],
})
export class AppModule {
}
