import { Body, Controller, Get, Post } from '@nestjs/common';

import { UsersService } from './users.service';

import { User } from './user.entity';

import { UserDto } from '../dtos/user.dto';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {
  }

  @Get()
  async getUsers(): Promise<User[]> {
    return await this.usersService.findUsers();
  }

  @Post()
  async postUser(@Body() userDto: UserDto): Promise<User> {
    return this.usersService.saveUser(userDto);
  }
}
