import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { User } from './user.entity';
import { UserDto } from '../dtos/user.dto';

@Injectable()
export class UsersService {

  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private logger: Logger,
  ) {

  }

  async findUsers(): Promise<User[]> {
    return await this.userRepository.find();
  }

  async saveUser(userDto: UserDto) {
    let user;

    try {
      user = await this.userRepository.save(userDto);
    } catch (e) {
      this.logger.error(`Error: ${e}`);
    }

    return user;
  }
}
