import {Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, Unique, UpdateDateColumn} from 'typeorm';

@Entity()
@Unique(['username'])
export class User {

  @PrimaryGeneratedColumn()
  id: number | undefined;

  @Column('varchar')
  username: string | undefined;

  @Column('varchar')
  password: string | undefined;

  @Column('varchar')
  name: string | undefined;

  @Column('varchar')
  state: string | undefined;

  @Column('varchar')
  role: string | undefined;

  @Column('datetime')
  @CreateDateColumn()
  createdAt: Date | undefined ;

  @Column('datetime')
  @UpdateDateColumn()
  updatedAt: Date | undefined;
}
