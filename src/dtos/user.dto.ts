export class UserDto {
  readonly name: string;
  readonly username: string;
  readonly state: string;
}
